<?php

namespace App\Tests\Core;

use App\Core\Card;
use App\Core\CardGame;
use PHPUnit\Framework\TestCase;

class CardGameTest extends TestCase
{

  public function testToString2Cards()
  {
    $jeudecarte = new CardGame([new Card('As', 'Pique'), new Card('Roi', 'Coeur')]);
    $this->assertEquals('CardGame : 2 carte(s)',$jeudecarte->__toString());
  }

  public function testToString1Card()
  {
    $jeudecarte = new CardGame([new Card('As', 'Pique')]);
    $this->assertEquals('CardGame : 1 carte(s)',$jeudecarte->__toString());
  }

  public function testCompare()
  {
      $card1 = new Card('5', 'As');
      $card2 = new Card('4', 'Coeur');
      $jeucarte = new CardGame([$card1, $card2]);
      $this->assertEquals(1, $jeucarte->compare($card1, $card2));
      $this->assertEquals(-1, $jeucarte->compare($card2, $card1));
  }

  public function testShuffle()
  {
      $card3 = new Card('3', 'Trefle');
      $card4 = new Card('4', 'Pique');
      $card5 = new Card('5', 'Coeur');
      $jeucarte = new CardGame([$card3, $card4, $card5]);
      $this->assertNotEquals($jeucarte, $jeucarte->shuffle());
  }

  public function testGetCard()
  {
      $card6 = new Card('6', 'Pique');
      $card7 = new Card('7', 'Coeur');
      $card8 = new Card('8', 'Trefle');
      $jeucarte = new CardGame([$card6, $card7, $card8]);
      $this->assertEquals($card7,$jeucarte->getCard(1));
  }

  public function testFactoryCardGame32()
  {

  }

}
